<?php
$servername = 'localhost';
$username = 'root';
$password = '';
$dbname = "p8_exercise_backend";

$con = mysqli_connect($servername, $username, $password, $dbname);

if (!$con) {
    die("Connection Failed" . mysqli_connect_error());
}