<?php
include './includes/header.php';
?>
<h1 class="text-center">PHP Basic Input Handling | Basic MySQL Queries</h1>

<!-- Create Employee Modal -->
<div class="modal fade" id="createEmployeesModal" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title w-100 text-center fs-5">Create Employee</h1>
            </div>
            <div class="modal-body">
                <form id="createEmployeeForm" class="row g-2">
                    <div class="col-sm-12">
                        <label for="createEmployeeFirstName" class="form-label fw-bold">First Name</label>
                        <input id="createEmployeeFirstName" type="text" class="form-control bg-light" name="createEmployeeFirstName" placeholder="Enter First Name.">
                    </div>
                    <div class="col-sm-12">
                        <label for="createEmployeeLastName" class="form-label fw-bold">Last Name</label>
                        <input id="createEmployeeLastName" type="text" class="form-control bg-light" name="createEmployeeLastName" placeholder="Enter Last Name.">
                    </div>
                    <div class="col-sm-12">
                        <label for="createEmployeeMiddleName" class="form-label fw-bold">Middle Name</label>
                        <input id="createEmployeeMiddleName" type="text" class="form-control bg-light" name="createEmployeeMiddleName" placeholder="Enter Middle Name.">
                    </div>
                    <div class="col-sm-12">
                        <label for="createEmployeeBirthday" class="form-label fw-bold">Birthday</label>
                        <input id="createEmployeeBirthday" type="date" class="form-control bg-light" name="createEmployeeBirthday" placeholder="Enter Birthday.">
                    </div>
                    <div class="col-sm-12">
                        <label for="createEmployeeAddress" class="form-label fw-bold">Address</label>
                        <input id="createEmployeeAddress" type="text" class="form-control bg-light" name="createEmployeeAddress" placeholder="Enter Address.">
                    </div>
                    <div class="modal-footer mt-2 pb-0 px-0">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- View Employee Modal -->
<div class="modal fade" id="viewEmployeesModal" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title w-100 text-center fs-5">View Employee</h1>
            </div>
            <div class="modal-body">
                <form id="viewEmployeeForm" class="row g-2">
                    <div class="col-sm-12">
                        <label for="viewEmployeeID" class="form-label fw-bold">ID</label>
                        <input id="viewEmployeeID" type="text" class="form-control bg-light" name="viewEmployeeID" readonly>
                    </div>
                    <div class="col-sm-12">
                        <label for="viewEmployeeFirstName" class="form-label fw-bold">First Name</label>
                        <input id="viewEmployeeFirstName" type="text" class="form-control bg-light" name="viewEmployeeFirstName" readonly>
                    </div>
                    <div class="col-sm-12">
                        <label for="viewEmployeeLastName" class="form-label fw-bold">Last Name</label>
                        <input id="viewEmployeeLastName" type="text" class="form-control bg-light" name="viewEmployeeLastName" readonly>
                    </div>
                    <div class="col-sm-12">
                        <label for="viewEmployeeMiddleName" class="form-label fw-bold">Middle Name</label>
                        <input id="viewEmployeeMiddleName" type="text" class="form-control bg-light" name="viewEmployeeMiddleName" readonly>
                    </div>
                    <div class="col-sm-12">
                        <label for="viewEmployeeBirthday" class="form-label fw-bold">Birthday</label>
                        <input id="viewEmployeeBirthday" type="date" class="form-control bg-light" name="viewEmployeeBirthday" readonly>
                    </div>
                    <div class="col-sm-12">
                        <label for="viewEmployeeAddress" class="form-label fw-bold">Address</label>
                        <input id="viewEmployeeAddress" type="text" class="form-control bg-light" name="viewEmployeeAddress" readonly>
                    </div>
                    <div class="modal-footer mt-2 pb-0 px-0">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Edit Employee Modal -->
<div class="modal fade" id="editEmployeesModal" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title w-100 text-center fs-5">Edit Employee</h1>
            </div>
            <div class="modal-body">
                <form id="editEmployeeForm" class="row g-2">
                    <input type="hidden" id="editEmployeeID" name="editEmployeeID">
                    <div class="col-sm-12">
                        <label for="editEmployeeFirstName" class="form-label fw-bold">First Name</label>
                        <input id="editEmployeeFirstName" type="text" class="form-control bg-light" name="editEmployeeFirstName" placeholder="Enter First Name.">
                    </div>
                    <div class="col-sm-12">
                        <label for="editEmployeeLastName" class="form-label fw-bold">Last Name</label>
                        <input id="editEmployeeLastName" type="text" class="form-control bg-light" name="editEmployeeLastName" placeholder="Enter Last Name.">
                    </div>
                    <div class="col-sm-12">
                        <label for="editEmployeeMiddleName" class="form-label fw-bold">Middle Name</label>
                        <input id="editEmployeeMiddleName" type="text" class="form-control bg-light" name="editEmployeeMiddleName" placeholder="Enter Middle Name.">
                    </div>
                    <div class="col-sm-12">
                        <label for="editEmployeeBirthday" class="form-label fw-bold">Birthday</label>
                        <input id="editEmployeeBirthday" type="date" class="form-control bg-light" name="editEmployeeBirthday" placeholder="Enter Birthday.">
                    </div>
                    <div class="col-sm-12">
                        <label for="editEmployeeAddress" class="form-label fw-bold">Address</label>
                        <input id="editEmployeeAddress" type="text" class="form-control bg-light" name="editEmployeeAddress" placeholder="Enter ADdress.">
                    </div>
                    <div class="modal-footer mt-2 pb-0 px-0">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Delete Employee Modal -->
<div class="modal fade" id="deleteEmployeesModal" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title w-100 text-center fs-5">Delete Employee?</h1>
            </div>
            <div class="modal-body">
                <form id="deleteEmployeeForm" class="row g-2">
                    <input type="hidden" id="deleteEmployeeID" name="deleteEmployeeID">
                    <p>Confirm deletion of <strong id="deleteEmployeeFirstName"></strong> <strong id="deleteEmployeeLastName"></strong>?</p>
                    <div class="modal-footer mt-2 pb-0 px-0">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Table Content -->
<div class="container-lg">
    <div class="row">
        <div class="col-md-12 pt-5">
            <div class="card">
                <div class="card-header">
                    <h4>
                        Pixel8 - Task No. 7
                        <button type="button" class="btn btn-primary float-end" data-bs-toggle="modal" data-bs-target="#createEmployeesModal">
                            Create Employee
                        </button>
                    </h4>
                </div>
                <div class="card-body">

                    <table id="employeeTable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Middle Name</th>
                                <th>Birthday</th>
                                <th>Address</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $query = "SELECT * FROM `employee`";
                            $query_run = mysqli_query($con, $query);

                            if (mysqli_num_rows($query_run) > 0) {
                                foreach ($query_run as $employee) {
                            ?>
                                    <tr>
                                        <td><?= $employee['id']; ?></td>
                                        <td><?= $employee['first_name']; ?></td>
                                        <td><?= $employee['last_name']; ?></td>
                                        <td><?= $employee['middle_name']; ?></td>
                                        <td><?= $employee['birthday']; ?></td>
                                        <td><?= $employee['address']; ?></td>
                                        <td>
                                            <button type="button" value="<?= $employee['id']; ?>" class="btn btn-info viewEmployeeButton">View</button>
                                            <button type="button" value="<?= $employee['id']; ?>" class="btn btn-success editEmployeeButton">Edit</button>
                                            <button type="button" value="<?= $employee['id']; ?>" class="btn btn-danger deleteEmployeeButton">Delete</button>
                                        </td>
                                    </tr>
                            <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

<?php
include './includes/footer.php';
?>