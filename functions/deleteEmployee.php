<?php
include '../includes/connection.php';

if (isset($_POST['deleteEmployeeButton'])){
    $id = mysqli_real_escape_string($con, $_POST['deleteEmployeeID']);

    $sql = "DELETE FROM `employee` WHERE `id` = '$id'";
    $result = mysqli_query($con, $sql);

    if ($result) {
        // Respone Status and Message Response
        $res = [
            'status' => 200, // Success Number
            'message' => 'Employee deleted successfully.'
        ];
        // Display message
        echo json_encode($res);
        return false;
    }
    else {
        // Respone Status and Message Response
        $res = [
            'status' => 400, // Error Number
            'message' => 'Employee could not be deleted at this moment. Please try again later.'
        ];
        // Display message
        echo json_encode($res);
        return false;
    }
}