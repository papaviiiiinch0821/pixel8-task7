<?php
include '../includes/connection.php';


if(isset($_GET['employeeID'])){
    $id = mysqli_real_escape_string($con, $_GET['employeeID']);

    $sql = "SELECT * FROM `employee` WHERE `id` = '$id'";
    $result = mysqli_query($con, $sql);

    if (mysqli_num_rows($result) == 1) {
        // Fetching data of employee into array
        $data = mysqli_fetch_array($result);

        // Respone Status and Message Response
        $res = [
            'status' => 200, 
            'message' => 'Employee fetched successfully by id.',
            'data' => $data 
        ];
        // Display the success message
        echo json_encode($res);
        return false;
    }
    else {
        // Respone Status and Message Response
        $res = [
            'status' => 400, 
            'message' => 'Employee details not found.'
        ];
        // Display the error message
        echo json_encode($res);
        return false;
    }
}