<?php
include '../includes/connection.php';

if(isset($_POST['createEmployeeButton'])){
    $first_name = mysqli_real_escape_string($con, $_POST['createEmployeeFirstName']);
    $last_name = mysqli_real_escape_string($con, $_POST['createEmployeeLastName']);
    $middle_name = mysqli_real_escape_string($con, $_POST['createEmployeeMiddleName']);
    $birthday = mysqli_real_escape_string($con, $_POST['createEmployeeBirthday']);
    $address = mysqli_real_escape_string($con, $_POST['createEmployeeAddress']);

    if ($first_name == NULL || $last_name == NULL || $middle_name == NULL || $birthday == NULL || $address == NULL){
        // Respone Status and Message Response
        $res = [
            'status' => 400, // Error Number
            'message' => 'Please complete the employee deatails to be created.'
        ];
        // Display the error message
        echo json_encode($res);
        return false;
    }

    $sql = "INSERT INTO `employee` (`first_name`, `last_name`, `middle_name`, `birthday`, `address`) 
            VALUES ('$first_name', '$last_name', '$middle_name', '$birthday', '$address')";
    $result = mysqli_query($con, $sql);

    if($result) {
        // Respone Status and Message Response
        $res = [
            'status' => 200, // Success Number
            'message' => 'New employee created.'
        ];
        // Display the success message
        echo json_encode($res);
        return false;
    }
    else {
        // Respone Status and Message Response
        $res = [
            'status' => 401, // Error Number
            'message' => 'Employe could not be created at the moment.'
        ];
        // Display the error message
        echo json_encode($res);
        return false;
    }
}