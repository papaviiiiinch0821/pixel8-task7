<?php
include '../includes/connection.php';

if(isset($_POST['updateEmployeeButton'])){
    $id = mysqli_real_escape_string($con, $_POST['editEmployeeID']);
    $first_name = mysqli_real_escape_string($con, $_POST['editEmployeeFirstName']);
    $last_name = mysqli_real_escape_string($con, $_POST['editEmployeeLastName']);
    $middle_name = mysqli_real_escape_string($con, $_POST['editEmployeeMiddleName']);
    $birthday = mysqli_real_escape_string($con, $_POST['editEmployeeBirthday']);
    $address = mysqli_real_escape_string($con, $_POST['editEmployeeAddress']);

    if ($first_name == NULL || $last_name == NULL || $middle_name == NULL || $birthday == NULL || $address == NULL){
        // Respone Status and Message Response
        $res = [
            'status' => 400, // Error Number
            'message' => 'Please complete the employee deatails to be updated.'
        ];
        // Display the error message
        echo json_encode($res);
        return false;
    }

    $sql = "UPDATE `employee`
            SET 
                `first_name` = '$first_name',
                `last_name` = '$last_name',
                `middle_name` = '$middle_name',
                `birthday` = '$birthday',
                `address` = '$address'
            WHERE
                `id` = '$id'";
    $result = mysqli_query($con, $sql);

    if($result) {
        // Respone Status and Message Response
        $res = [
            'status' => 200, // Success Number
            'message' => 'New employee details updated.'
        ];
        // Display the success message
        echo json_encode($res);
        return false;
    }
    else {
        // Respone Status and Message Response
        $res = [
            'status' => 401, // Error Number
            'message' => 'Employe could not be updated at the moment.'
        ];
        // Display the error message
        echo json_encode($res);
        return false;
    }
}